<?php
# libraries
require 'libs/bootstrap.php';
require 'libs/controller.php';
require 'libs/model.php';
require 'libs/view.php';
require 'libs/database.php';

# config
require 'config/paths.php';
require 'config/database.php';

new Bootstrap();
