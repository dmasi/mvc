<?php
class Bootstrap {
	
	function __construct(){

		# trim off trailing slashes and make array
		$url = isset($_GET['url']) ? $_GET['url'] : NULL;
		$url = rtrim($url, '/');
		$url = explode('/', $url);

		# handle no controller being passed in url
		if(empty($url[0])){
			require 'controllers/index.php';
			$controller = new Index();
			$controller->index();
			return FALSE;
		}

		# load our controller
		$file = 'controllers/' . $url[0] . '.php';
		if(file_exists($file)){
			require $file;
		} else {
			require 'controllers/error.php';
			$controller = new Error();
			return FALSE;
		}

		$controller = new $url[0];

		// calling methods
		if(isset($url[2])){
			if(method_exists($controller, $url[1])){
				$controller->{$url[1]}($url[2]);
			} else {
				$controller->index();
			}
		} else {
			if(isset($url[1])){
				# yep looks funky but translates to: $controller->function();
				$controller->{$url[1]}();
			} else {
				$controller->index();
			}
		}

	}

}
