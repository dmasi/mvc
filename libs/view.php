<?php

class View extends Controller{

	function __construct(){
		//echo 'this is the view';
	}

	public function render($name, $noInclude = FALSE){
		if($noInclude == TRUE){
			require 'views/' . $name . '.php';
		} else {
			require 'views/header.php';
			require 'views/' . $name . '.php';
			require 'views/footer.php';
		}
	}

}
