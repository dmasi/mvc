<?php

class Database extends PDO {

	public function __construct(){
		parent::__construct('mysql:host='. MYSQL_HOST .';dbname=' . MYSQL_DBNAME, MYSQL_USER, MYSQL_PASS);
	}
}
