<?php

class Controller {

	function __construct(){
		//echo 'Main Controller';
		$this->view = new View();
	}

	# include the model if it exists automatically
	public function loadModel($name){
		$path = 'models/' . $name . '_model.php';

		if(file_exists($path)){
			require 'models/' . $name . '_model.php';
		}
	}

}
