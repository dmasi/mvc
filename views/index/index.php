
            <div class="content">
                <h1>Introduction</h1>
                <p>This is a simple code mockup to learn how an MVC pattern is constructed.</p>

                <h1>Model</h1>
                <p>The model represents the storage and retrieval of data.  This is usually from a
                    database.  However, it could also be used for api calls.  Most developers
                    encapsulate their business logic here.</p>

                <h1>View</h1>
                <p>The view is responsible for the presentation of the data retrieved by the model.
                In our case this is html.</p>

                <h1>Controller</h1>
                <p>The controller is what controls the flow for the models and views to work together.
                    The controller receives client requests and instructs the model and views to perform
                their actions.</p>
            </div>