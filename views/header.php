<!doctype html>
<html>
    <head>
        <title>MVC development code base</title>
        <link rel="stylesheet" href="<?php echo URL; ?>public/css/reset.css"/>
        <link rel="stylesheet" href="<?php echo URL; ?>public/css/default.css"/>
        <script type="text/javascript" src="<?php echo URL; ?>public/js/jquery-1.7.2.min.js"></script>
    </head>
    <body>

        <div class="wrapper">
            <div class="header">
                <span>The Header</span>
            </div>
            <div class="navigation">
                <span>Navigation:</span>
                <span><a href="<?php echo URL; ?>index">Index</a></span>
                <span><a href="<?php echo URL; ?>error">Error</a></span>
            </div>
            